import { module, test } from 'qunit';
import { visit, currentURL } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';

module('Acceptance | create new app', function(hooks) {
  setupApplicationTest(hooks);

  test('creating a new app', async function(assert) {
    await visit('/');
    assert.equal(currentURL(), '/');
  });
});
