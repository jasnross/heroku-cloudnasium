import sinon from 'sinon';
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Component | create-app-button', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    await render(hbs`{{create-app-button}}`);

    assert.equal(this.element.textContent.trim(), 'Create New App');

    await render(hbs`
      {{#create-app-button}}
        template block text
      {{/create-app-button}}
    `);

    assert.equal(this.element.textContent.trim(), 'Create New App');
  });

  test('calls the onSubmit handler with existing value', async function(assert) {
    const onSubmitStub = sinon.stub();
    this.set('onSubmitStub', onSubmitStub);

    this.set('mockApp', {
      name: 'my mock name'
    })

    await this.render(hbs`
      {{create-app-button app=mockApp onSubmit=onSubmitStub}}
    `);

    this.$()
      .find('button')
      .click();

    assert.strictEqual(onSubmitStub.callCount, 1);
    assert.deepEqual(onSubmitStub.args, [[
      {name: 'my mock name'}
    ]]);
  })

  test('calls the onSubmit hander with data entered into the input field', async function(assert) {
    const onSubmitStub = sinon.stub();
    this.set('onSubmitStub', onSubmitStub);

    await this.render(hbs`
      {{create-app-button onSubmit=onSubmitStub}}
    `);

    this.$('input')
      .val('some test input')
      .trigger('input');
    this.$()
      .find('button')
      .click();

    assert.strictEqual(onSubmitStub.callCount, 1);
    assert.deepEqual(onSubmitStub.args, [[
      {name: 'some test input'}
    ]]);
  });
});
