import Service from '@ember/service';
import config from '../config/environment';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

const API_KEY = config.apiKey;

/*
 * Note: I started this out with customizing a RESTAdapter and RESTSerializer
 * for the Platform API, but since it would require a bit of customization to
 * adapt it to Ember I went ahead with a more direct path of calling the API
 * using ember-ajax instead. Of course, if desired it would be possible to
 * customize the serializer to work correctly with the Platform API.
 */

export default Service.extend({
  ajax: service(),

  // Note: We could extend ember-ajax's ajax service to bake these headers in
  // later on
  headers: computed(function() {
    return {
      Authorization: `Bearer ${API_KEY}`,
      Accept: 'application/vnd.heroku+json; version=3',
      'Content-Type': 'application/json',
    };
  }),

  async fetchApps() {
    const ajax = this.get('ajax');
    const headers = this.get('headers');
    const apps = await ajax.request('https://api.heroku.com/apps', {
      headers,
    });
    return apps;
  },

  async createApp(appData) {
    console.log(appData);
    const ajax = this.get('ajax');
    const headers = this.get('headers');
    const apps = await ajax.request('https://api.heroku.com/apps', {
      method: 'POST',
      headers,
      data: JSON.stringify(appData),
    });
    return apps;
  },
});
