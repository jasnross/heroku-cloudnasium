import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  tagName: '',

  // Stores the app data set by the component
  app: computed(() => ({})),

  // Will be called on submit with the app data as a paramter
  onSubmit: null,

  actions: {
    setAppName(name) {
      this.set('app.name', name);
    },

    createNewApp(e) {
      e && e.preventDefault();

      // TODO: Validate the name before submitting
      const app = this.get('app');
      return this.onSubmit(app);
    },
  },
});
