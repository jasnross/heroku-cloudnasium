import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Controller.extend({
  platformApi: service(),

  init() {
    this._super(...arguments);
    this._loadApps();
  },

  actions: {
    // Submits a new app to the Platform API
    async submitApp({ name }) {
      const platformApi = this.get('platformApi');
      await platformApi.createApp({
        name,
      });

      // TODO: Handle errors from the API, such as name already taken, etc.

      // Reload the apps to account for the newly created app
      await this._loadApps();
    },
  },

  // TODO: Could be a good option to move this data to a service if it's needed
  // across other pages. Additionally, if a loading spinner is introduced, then
  // an ember-concurrency Task could help
  async _loadApps() {
    const platformApi = this.get('platformApi');
    const appsArray = await platformApi.fetchApps();
    this.set('_apps', appsArray);
  },

  // Array of current apps to display, loaded by `_loadApps`
  _apps: computed(() => []),
});
